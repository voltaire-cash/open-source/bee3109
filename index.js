/*
This line loads the bitcore-lib-cash dependency made by BitPay Inc. 
The dependency is not required for connecting to and extracting data from a full Bitcoin Node,
but for manipulating data extracted from the Node.
*/

const bitcoreLib = require('bitcore-lib-cash');

var privateKey = new bitcoreLib.PrivateKey();
var publicKey = privateKey.toPublicKey();
var address = publicKey.toAddress();

console.log(privateKey,publicKey,address);

/* 
This line defines three functions, getBlock, getBlockCount and getBlockHash from an external file
node-functions.js
These functions are passed to three constants: getBlock, getBlockCount and getBlockHash.
*/

const { getBlock, getBlockCount, getBlockHash } = require('./node-functions');


/*
This line calls the above imported function getBlockCount.
Then must be used as this is a async function (blocking).
This function returns the number of blocks in the chain, which also is the height (id) of the highest block.
The value returned by getBlockCount() is passed into the constant 'height'
We then call getBLockHash to get the hash of this highest block
*/

getBlockCount().then(function(height){

	/*
	We now can log this value into the console
	and then get the hash of this heighest block
	*/
	
	console.log('Blockchain height is ---',height);
	
	getBlockHash(height).then(function(hash){
    
        console.log('Latest Blockhash is ---',hash);
    
    });
	
});

/*
And there we have it! The number of blocks in the Bitcoin Cash Blockchain, which is just the height of the latest block, and its hash.
*/
