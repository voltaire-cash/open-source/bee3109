# BEE3109

This project contains code snippets for interacting with a Bitcoin Cash node, and
also has required package to use the bitcore API.

## How it works

There is one file called `node-functions.js`. It stores functions which use the
`bitcoind-rpc` library. This library allows communication with a Bitcoin Cash node.
A file called `config.js` is used by `node-functions.js` which stores information
of the target node. The Business School is running a full Bitcoin Cash node to which
you will connect to.

Three simple functions have been written which use the `bitcoind-rpc` library to get information 
from the Bitcoin Cash node.

These functions are exposed and so should be used from the `index.js` file.
Example usage of the functions is included in this file.

This helps separation of logic and keeps the code clean.

You may notice that we have also included `bitcore-lib-cash`.

This can be used directly inside the `index.js` file to create private keys,
public keys, wallets and addresses using JavaScript.


## How to run

1. Install Node.js [here](https://nodejs.org/en/download/)
2. Download repository using link click in top right on this page
3. Extract/unpack download
3. Open terminal window from inside main folder `BE3109`
4. Type `node index.js`

## Useful links

[Bitcore API documentation](https://bitcore.io/api/lib) (use inside index.js)

[Bitcore RPC documentation](https://github.com/bitpay/bitcoind-rpc) (use node-functions.js inside index.js )

