const { node } = require('./config');

const RpcClient = require('bitcoind-rpc');
const rpc = new RpcClient(node);

function getBlockCount() {
    return new Promise((resolve, reject) => {
        rpc.getBlockCount((err, ret) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(parseInt(ret.result, 10));
        });
    });
}

function getBlockHash(height) {
    return new Promise((resolve, reject) => {
        rpc.getBlockHash(height, (err, ret) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(ret.result);
        });
    });
}
function getBlock(hash, mode) {
    return new Promise((resolve, reject) => {
        rpc.getBlock(hash, mode, (err, ret) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(ret.result);
        });
    });
}

module.exports = { getBlock, getBlockCount, getBlockHash };

